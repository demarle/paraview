## Tooltip to clipboard support

Hover selection modes (Hover cells on and Hover points on) now support
to copy the content of the tooltip to the clipboard by pressing Ctrl+C.
