## Enable Catalyst to use Fides Reader

Catalyst can now use the Fides reader, when using the ADIOS2 Inline engine.
ParaView also builds an ADIOS plugin for use with ADIOS v2.8.0 or later, that simplifies using Catalyst for ADIOS2-enabled codes.
Documentation for using ParaView Catalyst with the Fides reader is located in the [Fides User Guide](https://fides.readthedocs.io/en/latest/paraview/paraview.html).
